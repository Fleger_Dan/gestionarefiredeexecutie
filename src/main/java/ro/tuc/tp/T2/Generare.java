package ro.tuc.tp.T2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Generare 
{
	public int min;
	public int max;
	
	public int id=-1;
	public List<String> evenimente;
	public int secunde;
	public Generare(int min,int max)
	{
		evenimente=new ArrayList<String>();
		this.min=min;
		this.max=max;
	}
   public Client generez()
   {
	   
	   long timp=(long) (((Math.random()*((max-min)+1))+min)*1000);
	   try 
	   {
		Thread.sleep(timp);
	   }
	   catch (InterruptedException e) 
	   {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }
	   id++;
	   evenimente.add("secunde:"+secunde+":A venit un client cu id-ul="+id);
	   //System.out.println("secunde:"+secunde+":A venit un client cu id-ul="+id);
	return new Client(secunde,0,0,id);
   }
}
