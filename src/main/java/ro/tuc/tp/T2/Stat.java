package ro.tuc.tp.T2;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Stat
{
	public JFrame frame;
	public JTextField tastmin;
	public JTextField sermin;
	public JTextField goliremin;
	public Stat()
	{
		initializare();
	}
	public void initializare()
	{
		frame=new JFrame("Statistici");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 300, 150);
		frame.getContentPane().setLayout(null);

		JLabel setari=new JLabel("Statistici:");
		setari.setBounds(20, 0, 100, 20);
		frame.getContentPane().add(setari);

		// linie1
		JLabel tast=new JLabel("TimpMAsteptare");
		tast.setBounds(20, 20, 100, 20);
		frame.getContentPane().add(tast);

		 tastmin=new JTextField();
		tastmin.setEditable(false);
		tastmin.setBounds(120, 20, 100, 20);
		frame.getContentPane().add(tastmin);

		// linie2
		JLabel tser=new JLabel("TimpMServire");
		tser.setBounds(20, 40, 100, 20);
		frame.getContentPane().add(tser);

		 sermin=new JTextField();
		sermin.setEditable(false);
		sermin.setBounds(120, 40, 100, 20);
		frame.getContentPane().add(sermin);
       
		// linie3
		JLabel golire=new JLabel("TimpMGolire");
		golire.setBounds(20, 60, 100, 20);
		frame.getContentPane().add(golire);

		 goliremin=new JTextField();
		goliremin.setEditable(false);
		goliremin.setBounds(120, 60, 100, 20);
		frame.getContentPane().add(goliremin);

	
	}
}
