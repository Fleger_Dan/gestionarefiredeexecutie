package ro.tuc.tp.T2;

public class Client
{
    private int timp_intrare;
    private int timp_asteptare;
    private int timp_iesire;
    private int id;
    public Client(int timp_intrare,int timp_asteptare,int timp_iesire,int id)
    {
    	this.setTimp_intrare(timp_intrare);
    	this.setTimp_asteptare(timp_asteptare);
    	this.setTimp_iesire(timp_iesire);
    	this.setId(id);
    }
	public int getTimp_intrare() {
		return timp_intrare;
	}
	public void setTimp_intrare(int timp_intrare) {
		this.timp_intrare = timp_intrare;
	}
	public int getTimp_asteptare() {
		return timp_asteptare;
	}
	public void setTimp_asteptare(int timp_asteptare) {
		this.timp_asteptare = timp_asteptare;
	}
	public int getTimp_iesire() {
		return timp_iesire;
	}
	public void setTimp_iesire(int timp_iesire) {
		this.timp_iesire = timp_iesire;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
