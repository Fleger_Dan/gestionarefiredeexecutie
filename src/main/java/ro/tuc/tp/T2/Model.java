package ro.tuc.tp.T2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JTextField;


public class Model 
{
	public int nr_secunde_gol;
	public int creat=0;
	public int fir=0;
	public int secunde;
	private int simulare;
	public int min;
	public int max;
	public long suma_servire;
	public int suma_asteptare;
	public int suma_golire;
	public int gol_max;
	public int nr_scosi;
	
	public JTextField digital;
	public ConcurrentLinkedQueue<Client> coada= new ConcurrentLinkedQueue<Client>();; 
	public Client container;
	public boolean var;
	
	public List<String> evenimente;
	public Model(int secunde,Integer simulare,JTextField dig)
	{
		this.var=true;
		this.secunde=secunde;
		this.simulare=simulare.intValue();
		this.digital=dig;
	}
    public Model(int min,int max)
    {
    	nr_secunde_gol=0;
    	this.var=true;
    	evenimente=new ArrayList<String>();
    	suma_servire=0;
    	suma_golire=0;
    	nr_scosi=0;
    	this.min=min;
    	this.max=max;
    }
	public void gol()
	{
		while(var)
		{
			if(coada.isEmpty())
			{
				nr_secunde_gol=nr_secunde_gol+1;
				try 
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	public void extragere()
	{
		
			while(var)
			{
				if(coada.isEmpty())
				{
					//suma_golire=suma_golire+secunde-gol_max;
				}
				else
				{
					
					
					long timp=(long) (((Math.random()*((max-min)+1))+min)*1000);
					try 
					{
						Thread.sleep(timp);
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
					container=coada.remove();
					container.setTimp_iesire(secunde);
					container.setTimp_asteptare(container.getTimp_iesire()-container.getTimp_intrare());	
					nr_scosi++;
					suma_asteptare=suma_asteptare+container.getTimp_asteptare();
					suma_servire=suma_servire+timp/1000;
					evenimente.add("secunde "+secunde+":A fost servit clinetul id="+container.getId()+" timpI="+container.getTimp_intrare()+" timpO="+container.getTimp_iesire());
					gol_max=secunde;
					//System.out.println("secunde "+secunde+":A fost servit clinetul id="+container.getId()+" timpI="+container.getTimp_intrare()+" timpO="+container.getTimp_iesire());
				}
				
			}
		
	}
	
	
	
	
	public void ceas()
	{
		synchronized(this)
		{
			while(secunde<simulare)
			{
				secunde=secunde+1;
				try 
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
				digital.setText(secunde+"");
			}
			var=false;
			

		};
	}

	
}
