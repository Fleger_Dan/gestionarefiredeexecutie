package ro.tuc.tp.T2;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class Controller 
{
	public View v;
	public Stat s;
	public DefaultListModel <String> cozi;
	public DefaultListModel <String> evenimente;
	public int tastmax;
	public int tastmin;
	public int sermin;
	public int sermax;
	public int nr;
	public int capac;
	public int simulare;
	public Controller(View v)
	{

		s=new Stat();
		s.frame.setVisible(false);
		this.v=v;
		v.actiune1(new Funct1());
		v.actiune2(new Funct2());
	}
	public class Funct1 implements ActionListener
	{

		public void actionPerformed(ActionEvent e) 
		{
			if(s.frame.isVisible())
			{
				s.frame.setVisible(false);
			}
			else
			{
				s.frame.setVisible(true);
			}

		}
	}
	public class Funct2 implements ActionListener
	{

		public void actionPerformed(ActionEvent e) 
		{
			if(validare())
			{
				tastmax=Integer.parseInt(v.tastmax.getText());
				tastmin=Integer.parseInt(v.tastmin.getText());
				sermin=Integer.parseInt(v.sermin.getText());
				sermax=Integer.parseInt(v.sermax.getText());
				nr=Integer.parseInt(v.nr.getText());
				capac=Integer.parseInt(v.capac.getText());
				simulare=Integer.parseInt(v.simulare.getText());

				final Model ceas=new Model(0,simulare,v.sec);
				final Model angajati[]=new Model[nr];

				final Generare usa=new Generare(tastmin,tastmax);

				Thread tceas=new Thread(new Runnable()
				{

					public void run()
					{

						ceas.ceas();
					}

				});
				tceas.start();

				Thread organizare=new Thread(new Runnable()
				{

					public void run() 
					{
						Client client;
						int nrgencoada=1;
						int iprim=0;
						int min=Integer.MAX_VALUE;
						float medie1;
						float medie2;
						float medie3;
						for(int i=0;i<nr;i++)
						{
							angajati[i]=new Model(sermin,sermax);
							/*
							final int var=i;
							new Thread(new Runnable()
							{

								public void run()
								{
									angajati[var].extragere();

								}

							}).start();
							*/
						}
						new Thread(new Runnable()
						{

							public void run()
							{
								angajati[0].extragere();

							}

						}).start();
						new Thread(new Runnable()
						{

							public void run()
							{
								angajati[0].gol();

							}

						}).start();
						angajati[0].fir=1;
						while(ceas.var)
						{
							min=Integer.MAX_VALUE;
							cozi=new DefaultListModel<String>();
							evenimente=new DefaultListModel<String>();
							for(int i=0;i<nr;i++)
							{

								angajati[i].secunde=ceas.secunde;
								angajati[i].var=ceas.var;
								if(angajati[i].coada.size()<min && angajati[i].fir==1)
								{
									min=angajati[i].coada.size();
									iprim=i;
								}
								
								if(angajati[i].coada.size()>=capac && nrgencoada<nr && angajati[i].creat==0)
								{
									angajati[i].creat=1;
									final int gencoada=nrgencoada;
									new Thread(new Runnable()
									{

										public void run()
										{
											angajati[gencoada].extragere();

										}

									}).start();
									new Thread(new Runnable()
									{

										public void run()
										{
											angajati[gencoada].gol();

										}

									}).start();
									angajati[nrgencoada].fir=1;
									nrgencoada++;
									

								}
								
							}

							usa.secunde=ceas.secunde;
							client=usa.generez();
							angajati[iprim].coada.add(client);

							for(int i=0;i<nr;i++)
							{

									cozi.addElement("Coada "+(i+1));
									Iterator<Client> aux=angajati[i].coada.iterator();
									while(aux.hasNext())
									{
										Client cl=aux.next();
										cozi.addElement(cl.getTimp_asteptare()+":Clientul cu id-ul="+cl.getId());
									}

							
							}
							for(int i=0;i<nr;i++)
							{

						
									evenimente.addElement("Coada "+(i+1));
									for(int j=0;j<angajati[i].evenimente.size();j++)
									{
										evenimente.addElement(angajati[i].evenimente.get(j));
									}
							}
							evenimente.addElement("Inserare");
							for(int j=0;j<usa.evenimente.size();j++)
							{
								evenimente.addElement(usa.evenimente.get(j));
							}
							medie1=medie2=medie3=0;
							for(int i=0;i<nr;i++)
							{
								if(angajati[i].nr_scosi!=0)
								{
								medie1=medie1+angajati[i].suma_asteptare/angajati[i].nr_scosi;
								medie2=medie2+angajati[i].suma_servire/angajati[i].nr_scosi;
								}
								medie3=medie3+angajati[i].nr_secunde_gol;
							}
							medie1=medie1/nrgencoada;
							medie2=medie2/nrgencoada;
							medie3=medie3/nrgencoada;
							s.tastmin.setText(medie1+"");
							s.sermin.setText(medie2+"");
							if(medie3>=0)
							{
							s.goliremin.setText(medie3+"");
							}
							else
							{
							s.goliremin.setText("0");
							}
							v.lista1.setModel(cozi);
							v.lista2.setModel(evenimente);
						}

					}

				});
				organizare.start();
			}
			else
			{
				JOptionPane.showMessageDialog(null,"Configuratia nu este buna:\n introduceti numere pozitive si respectati intervalele");
			}

		}

	}
	public boolean validare()
	{
		int tastmax;
		int tastmin;
		int sermin;
		int sermax;
		int nr;
		int capac;
		int simulare;
		if(v.tastmax.getText().length()==0 || v.tastmin.getText().length()==0 || v.sermin.getText().length()==0 || v.sermax.getText().length()==0 || v.nr.getText().length()==0 || v.capac.getText().length()==0 || v.simulare.getText().length()==0)
		{
			return false;
		}
		else
		{
			if(digits(v.tastmax.getText()) && digits(v.tastmin.getText()) && digits(v.sermin.getText()) && digits(v.sermax.getText()) && digits(v.nr.getText()) && digits(v.capac.getText()) && digits(v.simulare.getText()) && true)
			{
				tastmax=Integer.parseInt(v.tastmax.getText());
				tastmin=Integer.parseInt(v.tastmin.getText());
				sermin=Integer.parseInt(v.sermin.getText());
				sermax=Integer.parseInt(v.sermax.getText());
				nr=Integer.parseInt(v.nr.getText());
				capac=Integer.parseInt(v.capac.getText());
				simulare=Integer.parseInt(v.simulare.getText());
			}
			else
			{
				return false;
			}

		}
		if(tastmin>=0 && tastmax>0 && sermin>=0 && sermax>0 && nr>0 && capac>0 && simulare>0)
		{
			if(tastmin>=tastmax && sermin>=sermax)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		return true;

	}
	public boolean digits(String s)
	{
		for(int i=0;i<s.length();i++)
		{
			if(!Character.isDigit(s.charAt(i)))
			{
				return false;
			}
		}
		return true;
	}
}
