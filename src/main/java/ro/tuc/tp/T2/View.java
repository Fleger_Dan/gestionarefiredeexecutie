package ro.tuc.tp.T2;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class View 
{
	public JFrame frame;
	public JList lista1;
	public JList lista2;

	public JButton statistici;
	public JButton start;

	public JTextField tastmin;
	public JTextField tastmax;
	public JTextField sermin;
	public JTextField sermax;
	public JTextField nr;
	public JTextField capac;
	public JTextField simulare;
	public JTextField sec;
	public View()
	{
		initializare();
	}
	public void initializare()
	{
		frame=new JFrame("Meniu Principal");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100,100, 900, 700);
		frame.getContentPane().setLayout(null);

		JLabel setari=new JLabel("Setari");
		setari.setBounds(20, 0, 100, 20);
		frame.getContentPane().add(setari);

		// linie1
		JLabel tast=new JLabel("TimpAsteptare");
		tast.setBounds(20, 20, 100, 20);
		frame.getContentPane().add(tast);

		 tastmin=new JTextField();
		tastmin.setBounds(120, 20, 100, 20);
		frame.getContentPane().add(tastmin);

		 tastmax=new JTextField();
		tastmax.setBounds(220, 20, 100, 20);
		frame.getContentPane().add(tastmax);

		// linie2
		JLabel tser=new JLabel("TimpServire");
		tser.setBounds(20, 40, 100, 20);
		frame.getContentPane().add(tser);

		 sermin=new JTextField();
		sermin.setBounds(120, 40, 100, 20);
		frame.getContentPane().add(sermin);

		 sermax=new JTextField();
		sermax.setBounds(220, 40, 100, 20);
		frame.getContentPane().add(sermax);

		// linie3
		JLabel nrcozi=new JLabel("NumarCozi");
		nrcozi.setBounds(20, 60, 100, 20);
		frame.getContentPane().add(nrcozi);

		 nr=new JTextField();
		nr.setBounds(120, 60, 100, 20);
		frame.getContentPane().add(nr);

		// linie4
		JLabel tsim=new JLabel("TimpSimulare");
		tsim.setBounds(20, 80, 100, 20);
		frame.getContentPane().add(tsim);

		 simulare=new JTextField();
		simulare.setBounds(120, 80, 100, 20);
		frame.getContentPane().add(simulare);

		// linie5
		JLabel cap=new JLabel("Capacitate");
		cap.setBounds(20, 100, 100, 20);
		frame.getContentPane().add(cap);

		 capac=new JTextField();
		capac.setBounds(120, 100, 100, 20);
		frame.getContentPane().add(capac);

		//tabel1

		JLabel ncozi1=new JLabel("ListaCozi");
		ncozi1.setBounds(20, 120, 100, 20);
		frame.getContentPane().add(ncozi1);

		JScrollPane scr=new JScrollPane();
		scr.setBounds(20, 140, 400, 400);
		frame.getContentPane().add(scr);
		lista1 = new JList();
		scr.setViewportView(lista1);

		//tabel2

		JLabel ncozi2=new JLabel("Evenimente");
		ncozi2.setBounds(420, 120, 100, 20);
		frame.getContentPane().add(ncozi2);

		JScrollPane scr1=new JScrollPane();
		scr1.setBounds(420, 140, 400, 400);
		frame.getContentPane().add(scr1);
		lista2 = new JList();
		scr1.setViewportView(lista2);

		statistici=new JButton("Statistica");
		statistici.setBounds(320, 20, 100, 20);
		frame.getContentPane().add(statistici);

		start=new JButton("Start");
		start.setBounds(320, 40, 100, 20);
		frame.getContentPane().add(start);
		
		JLabel afis=new JLabel("TimpAplicatie");
		afis.setBounds(420, 20, 100, 20);
		frame.getContentPane().add(afis);

		 sec=new JTextField();
		sec.setBounds(520, 20, 100, 20);
		sec.setEditable(false);
		frame.getContentPane().add(sec);

		frame.setVisible(true);
	}

	public void actiune1(ActionListener e)
	{
		statistici.addActionListener(e);
	}
	public void actiune2(ActionListener e)
	{
		start.addActionListener(e);
	}
}
